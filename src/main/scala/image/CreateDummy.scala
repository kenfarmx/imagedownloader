package image

/**
  * Created by ken on 2016/06/07.
  */

import org.opencv.core.{Core, MatOfRect, Point, Scalar}
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import org.opencv.core.Mat
import org.opencv.objdetect.CascadeClassifier

import geotrellis.engine.RasterSource
import geotrellis.raster._
import geotrellis.raster.io.geotiff
import geotiff.reader.GeoTiffReader.readMultiband
import geotrellis.raster.render._

import com.typesafe.config._

object CreateDummy extends App{
  System.loadLibrary(Core.NATIVE_LIBRARY_NAME)
  val config = ConfigFactory.load()

  val faceDetector = new CascadeClassifier(getClass.getResource("/lbpcascade_frontalface.xml").getPath)
  val image = Imgcodecs.imread(getClass.getResource("/lena.png").getPath)

  val faceDetections = new MatOfRect()
  faceDetector.detectMultiScale(image, faceDetections)

  println("Detected %s faces".format(faceDetections.toArray.size))

  faceDetections.toArray.foreach(rect => {
    Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0, 0))
  })

  val filename = "faceDetection.png"
  System.out.println(String.format("Writing %s", filename))

  Imgcodecs.imwrite(filename, image)

  val image_path = config.getString("imager.tiffpath") + "True_0001.tif"
  println(image_path)
  val tree_image = Imgcodecs.imread(image_path)
  val dst: Mat = tree_image
  Core.flip(tree_image, dst, -1)
  val flipname = "TreeFlip.tif"
  val tiff = readMultiband(image_path)
  println(tiff.extent)
  println(tiff.tile.band(0))
  println(tiff.tile.band(0).toArrayTile().asciiDraw())

  val array0 = tiff.tile.band(0).toArray()
  val array1 = tiff.tile.band(1).toArray()
  val array2 = tiff.tile.band(2).toArray()
  val array3 = tiff.tile.band(3).toArray()

  val tile = IntArrayTile(array0, tiff.raster.cols, tiff.raster.rows)
  tile.renderPng(ColorRamps.BlueToOrange).write("dummy.png")

  Imgcodecs.imwrite(flipname, dst)

}
