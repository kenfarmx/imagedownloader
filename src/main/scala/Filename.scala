/**
  * Created by ken on 2016/05/18.
  */
object Filename {
  def unapply(url: String): Option[String] = {
    url.split("/").reverse.toList.headOption
  }
}
