package models

/**
  * Created by ken on 2016/05/15.
  */

import java.util.regex.Pattern
import java.io.File

import com.typesafe.config._
import edu.uci.ics.crawler4j.crawler.{CrawlController, CrawlConfig, Page, WebCrawler}
import edu.uci.ics.crawler4j.fetcher.PageFetcher
import edu.uci.ics.crawler4j.parser.HtmlParseData
import edu.uci.ics.crawler4j.robotstxt.{RobotstxtServer, RobotstxtConfig}
import edu.uci.ics.crawler4j.url.WebURL


class SearchCrawler extends WebCrawler with Basic {

  val fileFilter = Pattern.compile(ignoreRegex);

  override def shouldVisit(page: Page, webURL: WebURL): Boolean = {
    // 調べてないけど、page にはリンク元ページが格納されてると思われ。
    // webURL の指定しているページをクロールするかどうかの判定処理。
    val currentUrl = webURL.getURL.toLowerCase
    !fileFilter.matcher(currentUrl).matches() && currentUrl.startsWith(baseUrl)
  }

  override def visit(page: Page): Unit = {
    // クロールした結果を受け取るハンドラ
    import scala.collection.JavaConversions._

    val currentUrl = page.getWebURL
    if (page.getParseData.isInstanceOf[HtmlParseData]) {
      val data = page.getParseData.asInstanceOf[HtmlParseData]
      val allTextData = data.getText
      val allLink = data.getOutgoingUrls

      println(s"CurrentURL  : $currentUrl")
      println(s"CurrentText : ${allTextData.length}")
      println(s"Links :")

      allLink.foreach(url => {
        println(s"Link : ${url.getURL}, ${url.getAnchor}, ${url.getTag}")
      })
    }
  }
}

object StartCrawler extends Basic {

  val crawlConfig = {
    val conf = new CrawlConfig
    conf.setCrawlStorageFolder(cache)
    conf
  }

  val controller = {
    val fetcher = new PageFetcher(crawlConfig)
    val robotTextConf = new RobotstxtConfig
    val robotTextServer = new RobotstxtServer(robotTextConf, fetcher)
    new CrawlController(crawlConfig, fetcher, robotTextServer)
  }

  def start(): Unit = {
    controller.addSeed(baseUrl)
    controller.start(classOf[SearchCrawler], threads)
  }
}
