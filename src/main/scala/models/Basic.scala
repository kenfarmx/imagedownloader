package models

/**
  * Created by ken on 2016/05/16.
  */

import com.typesafe.config._

trait Basic {
  val Configure = ConfigFactory.load().getConfig("crawler")
  val baseUrl = Configure.getString("baseUrl")
  val ignoreRegex = Configure.getString("ignoreRegex")
  val cache = Configure.getString("cache")
  val threads = Configure.getInt("threads")
}
