/**
  * Created by ken on 2016/05/15.
  */

import dispatch._
import dispatch.Defaults._
import scalax.io._

object Main {
  def main(args: Array[String]) = {
    val URL = "https://maps.googleapis.com/maps/api/staticmap?center=35.4965959079812,-119.15606668851&size=640x480&zoom=20&maptype=satellite&scale=1&FORMAT=image/png&&SRS=EPSG:3857"
    val h = new Http
    val requetstWithHandler =
      url(URL)
      .OK { response =>
        save(URL)
        println("OK")
      }

    h(requetstWithHandler)
    println("finish")
  }

  def save(url: String): Unit = {
    val data = Resource.fromURL(url).byteArray
    url match {
      case Filename(file) => Resource.fromFile(new java.io.File("data", file)).write(data)
      case _ => sys.error("Oops!")
    }
  }

}
